exports.handler = function(event, context, callback) {
    const data = JSON.parse(event.body);
    const { user } = data;

    const validateUser = email => {
        const domain = email.split("@")[1];
        switch(domain) {
            case 'propalt.co.uk':
                return ["Super User"];
            case 'gmail.com':
                return ['Test','Agent'];
            case 'serco.com':
                return ["Serco", 'Agent'];
            case 'g2mgroup.co.uk':
                return ["G2M", 'Agent'];
            default:
                return ['Agent'];
        }
    };
    
    const roles = validateUser(user.email);

    const responseBody = {
        app_metadata: { roles: roles },
        user_metadata: {
            ...user.user_metadata,
            custom_data_from_function: "some extra super data"
        }
    };

    callback(null, {
        statusCode: 200,
        body: JSON.stringify(responseBody)
    });
  
};