import React, { Fragment, useState, useRef, useEffect } from 'react';

export default props => {
  const [openingMarketList, openMarketList] = useState(false);
  const [openingProductList, openProductList] = useState(false);
  const [openingMobileNavigation, openMobileNavigation] = useState(false);
  const marketDropdown = useRef(null);
  const productDropdown = useRef(null);

  const handleToggleMenu = (menu) => {
    if(menu === 'marketing' && openingProductList) {
      openMarketList(true);
      openProductList(false);
      return;
    }
    if(menu === 'product' && openingMarketList) {
      openMarketList(false);
      openProductList(true);
      return;
    }
    return menu === 'marketing' ? openMarketList(!openingMarketList) : openProductList(!openingProductList);
  };

  const handleClickOutside = ({target}) => {
    if(!marketDropdown.current.contains(target) && !productDropdown.current.contains(target)) {
      openMarketList(false);
      openProductList(false);
    }
  };

  useEffect(
    () => {
      document.addEventListener('mousedown', handleClickOutside);
      return () => {
        document.removeEventListener('mousedown', handleClickOutside);
      }
    }
  , []);
  return (
    <Fragment>
      <div className="main-navigation">
          <nav className="navbar navbar-inverse bg-inverse navbar-toggleable-sm navbar-expand-lg">
            <a title="Propalt.co.uk Logo" className="logo navbar-brand">
              <a href='/'><img src="/static/logo.png" alt="Logo"/></a>
            </a>
            <div className='mobile-hamberger-button' onClick={() => openMobileNavigation(!openingMobileNavigation)}>
              <i className="fas fa-bars"></i>
            </div>
            <div className="collapse navbar-collapse justify-content-md-end desktop-navigation" id="navbarSupportedContent">
              <ul className="nav navbar-nav navbar-right">
                  <li className="nav-item" ref={marketDropdown}>
                      <a className="nav-link dropdown-toggle" onClick={() => handleToggleMenu('marketing')}>Market Sectors</a>
                      <div className={`dropdown-menu dropdown-primary product-menu ${openingMarketList ? 'show' : ''}`}>
                        <a href='/estate-agents' className="dropdown-item">Agents & Developers</a>
                        <a href='/household-products' className="dropdown-item">Household Goods</a>
                        <a href='/fintech-insurtech' className="dropdown-item">Fintech & Insurtech</a>
                        <a href='/proptech' className="dropdown-item">Proptech</a>
                        <a href='/data-dictionary' className="dropdown-item">Our Data</a>
                        <a href='/case-studies' className="dropdown-item">Case Studies</a>
                      </div>
                  </li>
                  <li className="nav-item" ref={productDropdown}>
                      <a className="nav-link dropdown-toggle" onClick={() => handleToggleMenu('product')}>Products</a>
                      <div className={`dropdown-menu dropdown-primary product-menu right-align-menu ${openingProductList ? 'show' : ''}`}>
                          <a href='/protect' className="dropdown-item">:Protect</a>
                          {/* <a href='/perform' className="dropdown-item">:Perform</a>
                          <a href='/professional' className="dropdown-item">:Professional</a>
                          <a href='/platform' className="dropdown-item">:Platform</a>
                          <a href='/persona' className="dropdown-item">:Persona</a> */}
                          <a href='/populate' className="dropdown-item">:Populate</a>
                          <a href='/validate' className="dropdown-item">:Validate</a>
                          <a href='/property-insights' className='dropdown-item'>:insights</a>
                          {/*<a href='/prospector-tool-product' className="dropdown-item">Prospector Tool</a>*/}
                      </div>
                  </li>
                  <li className="nav-item">
                      <a href='/about-us' className="dropdown-item">About Us</a>
                  </li>
                  <li className="nav-item">
                      <a href='/about-us#team' className="dropdown-item">Our Team</a>
                  </li>
              </ul>
            </div>
          </nav>
          <div className={`mobile-navigation ${openingMobileNavigation ? 'active' : 'inactive'}`}>
            <ul className="nav navbar-nav navbar-right">
                <li className="nav-item">
                    <a className="nav-link top-section">Market Sectors</a>
                    <div className='menu-items'>
                      <a href='/estate-agents' className="dropdown-item">Agents & Developers</a>
                      <a href='/household-products' className="dropdown-item">Household Goods</a>
                      <a href='/fintech-insurtech' className="dropdown-item">Fintech & Insurtech</a>
                      <a href='/proptech' className="dropdown-item">Proptech</a>
                      <a href='/data-dictionary' className="dropdown-item">Our Data</a>
                      <a href='/case-studies' className="dropdown-item">Case Studies</a>
                    </div>
                </li>
                <li className="nav-item">
                    <a className="nav-link top-section">Products</a>
                    <div className='menu-items'>
                        <a href='/protect' className="dropdown-item">:Protect</a>
                        {/* <a href='/perform' className="dropdown-item">:Perform</a>
                        <a href='/professional' className="dropdown-item">:Professional</a>
                        <a href='/platform' className="dropdown-item">:Platform</a>
                        <a href='/persona' className="dropdown-item">:Persona</a> */}
                        <a href='/populate' className="dropdown-item">:Populate</a>
                        <a href='/validate' className="dropdown-item">:Validate</a>
                        <a href='/property-insights' className='dropdown-item'>:insights</a>
                        {/*<a href='/prospector-tool-product' className="dropdown-item">Prospector Tool</a>*/}
                      {/*<a href='/about-us' className="dropdown-item">About Us</a>*/}
                      {/*<a href='/about-us#team' className="dropdown-item">Our Team</a>*/}
                    </div>
                </li>
                <li className="nav-item">
                    <a href='/about-us' className="nav-link top-section">About Us</a>
                </li>
                <li className="nav-item">
                    <a href='/about-us#team' className="nav-link top-section">Our Team</a>
                </li>
            </ul>
          </div>
        </div>
        {props.children}
        <footer className="footer">
          <div className="container">
            <div className='row'>
              <div className="col-md-12">
                <div className='row'>
                  <div className="col-md-4 footer-links">
                      <h4>Sectors Served</h4>
                      <ul>
                          <li><a href="https://valuation.housevault.co.uk">Consumers</a></li>
                          <li><a href="/estate-agents">Estate Agency</a></li>
                          <li><a href="/finance-insurance-providers">Finance & Insurance</a></li>
                          <li><a href="/property-developers">Housebuilders & Developers</a></li>
                          <li><a href="/property-investors">Private & Institutional Investors</a></li>
                      </ul>
                  </div>
                  <div className="col-md-4 footer-links">
                      <h4>Company</h4>
                      <ul>
                          <li><a href="/about-us">About us</a></li>
                          {/*<li><a href="/register">Registration</a></li>*/}
                          {/*<li><a href="/login">Login</a></li>*/}
                          <li><a href="/terms-of-use">Terms & Conditions</a></li>
                          <li><a href="/privacy-policy">Privacy Policy</a></li>
                      </ul>
                  </div>
                  <div className="col-md-4 footer-links">
                      <h4>Contact us</h4>
                      <ul>
                          <li><a href="mailto:info@propalt.co.uk">info@propalt.co.uk</a></li>
                          <li>Tel: 020 3290 7670</li>
                          <li>
                              19 Nab Wood Terrace, <br/>
                              BD18 4HU, <br/>
                              UK</li>

                      </ul>
                  </div>
                </div>
              </div>
              </div>
          </div>
          <div className="blank-footer">
            <div className='container'>
              <div className='row'>
                <div className='col-md-12'>
                  <div className='copyright-container'>
                    <p className="copyright">© Propalt Ltd. All rights reserved {new Date().getFullYear()}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>
    </Fragment>
  );
}
