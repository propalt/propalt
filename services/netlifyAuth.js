import netlifyIdentity from 'netlify-identity-widget'


const netlifyAuth = {
  isAuthenticated: false,
  user: null,
  initialize(callback) {
    window.netlifyIdentity = netlifyIdentity
    netlifyIdentity.on('init', (user) => {
      callback(user)
    })
    netlifyIdentity.init()
  },
  authenticate(callback) {
    this.isAuthenticated = true
    netlifyIdentity.open()
    netlifyIdentity.on('login', (user) => {
      this.user = user
      const {
        app_metadata, created_at, confirmed_at, email, id, user_metadata
      } = netlifyIdentity.currentUser();
      localStorage.setItem(
        "currentUser",
        JSON.stringify({...app_metadata, created_at, confirmed_at, email, id, ...user_metadata})
      );

      callback(user)
      netlifyIdentity.close()
    })
  },
  signout(callback) {
    this.isAuthenticated = false
    netlifyIdentity.logout()
    netlifyIdentity.on('logout', () => {
      this.user = null
      localStorage.removeItem("currentUser");
      callback()
    })
  },
}

export default netlifyAuth